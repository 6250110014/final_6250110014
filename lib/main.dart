import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Final Test:Firebase CRUD',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text fields' controllers
  final TextEditingController _student_id = TextEditingController();
  final TextEditingController _firstname = TextEditingController();
  final TextEditingController _lastname = TextEditingController();
  final TextEditingController _major = TextEditingController();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _students =
  FirebaseFirestore.instance.collection('students');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _student_id.text = documentSnapshot['student_id'];
      _firstname.text = documentSnapshot['firstname'];
      _lastname.text = documentSnapshot['lastname'];
      _major.text = documentSnapshot['major'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                  controller: _student_idController,
                  decoration: const InputDecoration(
                    labelText: 'Student_ID',
                  ),
                ),
                TextField(
                  controller: _firstnameController,
                  decoration: const InputDecoration(labelText: 'student id'),
                ),
                TextField(
                  controller: _lastnameController,
                  decoration: const InputDecoration(labelText: 'firstname'),
                ),
                TextField(
                  controller: _majorController,
                  decoration: const InputDecoration(labelText: 'lastname'),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String firstname = _firstnameController.text;
                    final String lastname = _lastnameController.text;
                    final String major = _majorController.text;
                    final double student_id =
                    double.tryParse(_student_idController.text);
                    if (student_id != null && student_id != null) {
                      if (action == 'create') {
                        // Persist a new student to Firestore
                        await _students
                            .add({
                          "firstname": firstname,
                          "lastname": lastname,
                          "major": major,
                          "student_id": student_id,
                        })
                            .then((value) => print("Student Added"))
                            .catchError((error) =>
                            print("Failed to add student: $error"));
                      }

                      if (action == 'update') {
                        // Update the student
                        await _students
                            .doc(documentSnapshot.id)
                            .update({
                          "firstname": firstname,
                          "lastname": lastname,
                          "major": major,
                          "student_id": student_id,
                        })
                            .then((value) => print("Student Updated"))
                            .catchError((error) =>
                            print("Failed to update student: $error"));
                      }

                      // Clear the text fields
                      _firstnameController.text = '';
                      _lastnameController.text = '';
                      _majorController.text = '';
                      _student_idController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _students
        .doc(productId)
        .delete()
        .then((value) => print("Student Deleted"))
        .catchError((error) => print("Failed to delete student: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a student')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Final Test:Firebase CRUD'),
      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _students.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                streamSnapshot.data.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['student_id']),
                    subtitle: Text(documentSnapshot['firstname']),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}